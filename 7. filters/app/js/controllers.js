'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('MyCtrl1', ['$scope', function ($scope) {
      $scope.name = 'Dawid Nowak';
      $scope.date = new Date();
      $scope.myText = '';
  }])
  .controller('MyCtrl2', ['$scope', '$filter', function ($scope, $filter) {
      $scope.people = [
              { name: 'Tomek', tel: '+48 555 1212', age: 27, position: 'worker' },
              { name: 'Grzesiek', tel: '+48 555 1215', age: 27, position: 'boss' },
              { name: 'Szymon', tel: '+48 555 1218', age: 18, position: 'worker' }];


      $scope.myText = '';


      $scope.$watch('myText', function (value) {
          $scope.resultName = $filter('namefilter')($scope.people, value);
      });

  }]);
