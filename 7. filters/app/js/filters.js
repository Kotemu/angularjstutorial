'use strict';

/* Filters */

angular.module('myApp.filters', [])
   //source: http://jsfiddle.net/tUyyx/
  .filter('truncate', function () {
      return function (text, length, end) {
          if (isNaN(length))
              length = 10;

          if (!end)
              end = "...";
 
          if (text.length <= length || text.length - end.length <= length) {
              return text;
          }
          else {
              return String(text).substring(0, length-end.length) + end;
          }
 
      };
  })

  .filter('namefilter', function() {
    return function(people, text) {
        var items = {
            out: []
        };
        angular.forEach(people, function (value, key) {
            if (value.name.toLowerCase().indexOf(text.toLowerCase()) > -1 || value.tel.toLowerCase().indexOf(text.toLowerCase()) > -1) {
                this.out.push(value);
            }
        }, items);
        return items.out;
    }
  })

//source: http://thecodebarbarian.wordpress.com/2014/01/17/the-8020-guide-to-writing-and-using-angularjs-filters/
.filter('pluralize', function () {
    return function (ordinal, noun) {
        if (ordinal == 1) {
            return ordinal + ' ' + noun;
        } else {
            var plural = noun;
            if (noun.substr(noun.length - 2) == 'us') {
                plural = plural.substr(0, plural.length - 2) + 'i';
            } else if (noun.substr(noun.length - 2) == 'ch' || noun.charAt(noun.length - 1) == 'x' || noun.charAt(noun.length - 1) == 's') {
                plural += 'es';
            } else if (noun.charAt(noun.length - 1) == 'y' && ['a', 'e', 'i', 'o', 'u'].indexOf(noun.charAt(noun.length - 2)) == -1) {
                plural = plural.substr(0, plural.length - 1) + 'ies';
            } else if (noun.substr(noun.length - 2) == 'is') {
                plural = plural.substr(0, plural.length - 2) + 'es';
            } else {
                plural += 's';
            }
            return ordinal + ' ' + plural;
        }
    };
});
