'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('CarCtrl', ['$scope',
        function($scope) {
            $scope.name = 'Car';
            $scope.variable = 1;
            $scope.clickme = function() {
                alert('Car ctrl function called');
            }

            $scope.logThisAndScope = function () {
                console.log(this, $scope);
            }
        }


    ])
    .controller('ToyotaCtrl', ['$scope', 
        function ($scope) {
           $scope.name = 'Toyota';
        }
    ]);

//$injector