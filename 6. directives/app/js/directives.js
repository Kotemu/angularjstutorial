'use strict';

/* Directives */


angular.module('myApp.directives', [])
    .directive('appVersion', [
        'version', function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }
    ])
    .directive('personData', function() {
        return {
            template: 'personData directive: Name: {{person.name}}, Age: {{person.age}}, Position: {{person.position}}'
        }
    })
    .directive('personDataShort', function() {
        return {
            //'A' - only matches attribute name
            //'E' - only matches element name
            //'C' - only matches class name
            restrict: 'E',
            template: '<div>personDataShort directive: Name: {{person.name}}, Age: {{person.age}}, Position: {{person.position}}</div>'
        }
    })
    .directive('personDataComplex', function() {
        return {
            restrict: 'E',
            scope: {
                people: '=' // '@', '&'
            },
            template: '<ul><li ng-repeat="person in people">personDataComplex directive: Name: {{person.name}}, Age: {{person.age}}, Position: {{person.position}}</li></ul>'
        }
    })
    .directive('dateTimeFormat', [
        '$interval', 'dateFilter', function($interval, dateFilter) {

            function link(scope, element, attrs) {
                var format,
                    timeoutId;

                function updateTime() {
                    element.text(dateFilter(new Date(), format));
                }

                scope.$watch(attrs.dateTimeFormat, function(value) {
                    format = value;
                    updateTime();
                });

                element.on('$destroy', function() {
                    $interval.cancel(timeoutId);
                });

                // start the UI update process; save the timeoutId for canceling
                timeoutId = $interval(function() {
                    updateTime(); // update DOM
                }, 1000);
            }

            return {
                link: link
            };
        }
    ])
    .directive('welcomeDialog', function() {
        return {
            restrict: 'E',
            transclude: true,
            template: '<div ng-transclude></div>',
            scope: {},
            link : function(scope) {
                scope.name = 'Tomas';
            }
        };
    })
    .directive('myButton', function() {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                'action': '&onClick'
            },
            template: '<button style="height:100px; width: 300px;" ng-click="action()"><div ng-transclude></div></button>'
        };
    });

// Event listeners
// Directives that communicate
// Compile
