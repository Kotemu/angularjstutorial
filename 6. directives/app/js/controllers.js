'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('MyCtrl1', ['$scope', function ($scope) {
        $scope.people = [
                { name: 'Tomek', tel: '+48 555 1212', age: 27, position: 'worker' },
                { name: 'Grzesiek', tel: '+48 555 1215', age: 27, position: 'boss' },
                { name: 'Szymon', tel: '+48 555 1218', age: 18, position: 'worker' }];
    }])
    .controller('MyCtrl2', ['$scope', function ($scope) {
        $scope.format = 'M/d/yy h:mm:ss a';
        $scope.name = 'Lucas';
        $scope.resetFormat = function() {
            $scope.format = 'M/d/yy h:mm:ss a';
        };
    }])
    .controller('MyCtrl3', ['$scope', function ($scope) {
        $scope.name = 'John';
}]);
