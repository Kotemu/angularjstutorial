'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('MainCtrl', function ($scope, $route, $routeParams, $location) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
    })
    .controller('MyCtrl1', [function () {

    }])
    .controller('MyCtrl2', [function () {

    }]);
